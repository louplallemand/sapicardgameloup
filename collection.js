collection()

function collection (){
    let collection = JSON.parse(localStorage.getItem("data"))   
    let count = 0

    collection.forEach(element => {
        count++
        let div = document.createElement("div")
        div.className = "m-3 text-light m-3 border border-light border-3 p-3"
        let newText = document.createTextNode(element.name)
        div.appendChild(newText)
        let newImage = document.createElement('img')
        newImage.src = element.image
        newImage.className="imgmobile"
        div.appendChild(newImage) 
        let div1 = document.getElementById('div1')
        div1.appendChild(div)
    });
}
