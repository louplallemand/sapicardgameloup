let data = []
if (JSON.parse(localStorage.getItem("data")) !== null){
    data = JSON.parse(localStorage.getItem("data"))
}
let trueTime = 0
let time = 0
if (JSON.parse(localStorage.getItem('timer')) !== null){
    time = JSON.parse(localStorage.getItem('timer'))
    trueTime = (60 *60 *2) - (Math.round((Date.now() - time)/1000))
}


function getCard() {
    let type = ["people", "planets", "species", "starships", "vehicles"]
    let max = [82,60,37,36,39]
    let randomType = Math.floor(Math.random()* 5)
    let randomNum = Math.floor(Math.random()*max[randomType])+1
    fetch(`https://swapi.dev/api/${type[randomType]}/${randomNum}`).then((response) => 

    response.json().then((data) => {
        if (data.detail !== "Not found"){
            let card = new Object
            card.name = data.name
            card.image = `./assets/${type[randomType]}/${randomNum}.jpg`
            document.getElementById('name').innerHTML = card.name
            document.getElementById('cardImage').src = card.image
            saveCard(card)
        }else{
            console.log("DEATH")
            getCard()
        }

    }))
}

function saveCard (card){
    let dataJ = JSON.stringify(data)
    if (!dataJ.includes(JSON.stringify(card))){
        data.push(card)
        dataJ = JSON.stringify(data)
        localStorage.setItem("data",dataJ)
    }
}

function setTimer (){
    let timer = Date.now()
    localStorage.setItem('timer',JSON.stringify(timer))
    time = timer
    trueTime = 60 *60 *2
}
function timer () {
    let deuxH = 1000 * 60 *60 *2
    let timer = JSON.parse(localStorage.getItem('timer'))

    if (timer == null && time !== 0){
        localStorage.setItem('timer', JSON.stringify(time))
    }
    timer = JSON.parse(localStorage.getItem('timer'))
    let currentTime = Date.now()

    if (currentTime >= timer+deuxH){
        getCard()
        setTimer()
    }
    
}

function getTimer () {
    if (trueTime > 0) {
    let seconde = trueTime%60

    let hour = 2

    if ((trueTime - (60*60*2)) < 0){
        hour = 0
    }
    if ((trueTime - (60*60)) < 3600 && (trueTime - (60*60)) >= 0 ){
        hour = 1
    }

    let minute = 0
    if(hour == 1){
        minute = trueTime - 3600 
    }
    if (hour == 2){
        minute = trueTime - 7200
    }
    if (hour == 0){
        minute = trueTime 
    }
    minute = Math.floor(minute/60)


    document.getElementById('time').innerHTML = `${hour} h : ${minute} m : ${seconde} s`;
    }else{
        document.getElementById('time').innerHTML = `READY`;
    }
    trueTime -= 1
    setTimeout(() => { getTimer();}, 1000)
}

getTimer()